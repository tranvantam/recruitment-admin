import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from './material/material.module';
import { ConfirmDialog } from './dialogs/confirm/confirm.dialog';
import { SpinnerDialog } from './dialogs/spinner/spinner.dialog';


@NgModule({
  declarations: [
    ConfirmDialog,
    SpinnerDialog
  ],
  imports: [
    CommonModule,
    MaterialModule,
  ],
  exports: [
    MaterialModule,
    ConfirmDialog,
    SpinnerDialog
  ]
})
export class SharedModule { }
