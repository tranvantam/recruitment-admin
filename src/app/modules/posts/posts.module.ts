import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CKEditorModule } from 'ngx-ckeditor';

import { SharedModule } from 'src/app/shared/shared.module';
import { PostsPage } from 'src/app/pages/posts/posts.page';
import { PostsDetailDialog } from 'src/app/pages/posts/dialogs/posts-detail/posts-detail.dialog';
import { PostsCreateDialog } from 'src/app/pages/posts/dialogs/posts-create/posts-create.dialog';
import { PostsEditDialog } from 'src/app/pages/posts/dialogs/posts-edit/posts-edit.dialog';

const routes : Routes = [
  {
    path: '',
    component: PostsPage
  }
]

@NgModule({
  declarations: [
    PostsPage,
    PostsDetailDialog,
    PostsCreateDialog,
    PostsEditDialog,
  ],
  imports: [
    CommonModule,
    CKEditorModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    RouterModule.forChild(routes)
  ]
})
export class PostsModule { }
