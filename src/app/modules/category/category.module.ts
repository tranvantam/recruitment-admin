import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { SharedModule } from 'src/app/shared/shared.module';
import { CategoryPage } from 'src/app/pages/category/category.page';
import { CategoryEditDialog } from 'src/app/pages/category/dialogs/category-edit/category-edit.dialog';
import { CategoryCreateDialog } from 'src/app/pages/category/dialogs/category-create/category-create.dialog';
import { CategoryDetailDialog } from 'src/app/pages/category/dialogs/category-detail/category-detail.dialog';

const routes : Routes = [
  {
    path: '',
    component: CategoryPage
  }
]

@NgModule({
  declarations: [CategoryPage, CategoryEditDialog, CategoryCreateDialog, CategoryDetailDialog],
  imports: [
    CommonModule,
    SharedModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)
  ]
})
export class CategoryModule { }
