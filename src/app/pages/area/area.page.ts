import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AreaService } from 'src/app/modules/area/services/area.service';
import { Area } from 'src/app/core/models/area';
import { AreaDetailDialog } from './dialogs/area-detail/area-detail.dialog';
import { AreaCreateDialog } from './dialogs/area-create/area-create.dialog';
import { ConfirmDialog } from 'src/app/shared/dialogs/confirm/confirm.dialog';
import { AreaEditDialog } from './dialogs/area-edit/area-edit.dialog';
import { SpinnerDialog } from 'src/app/shared/dialogs/spinner/spinner.dialog';

@Component({
  selector: 'app-area',
  templateUrl: './area.page.html',
  styles: [
  ]
})
export class AreaPage implements OnInit {
  listAreas: Array<Area>;
  
  constructor(private areaService: AreaService, public dialog: MatDialog) { }

  ngOnInit(): void {
    this.getList();
  }   

  getList() {
    this.areaService.getList().subscribe(res => {
      this.listAreas = res;          
    })
  }

  onCreate() {
    let dialogData = this.dialog.open(AreaCreateDialog, {
      minWidth: 450
    });
    dialogData.afterClosed().subscribe(res => {
      this.getList();
    })
  }

  onDetail(area: Area) {
    this.dialog.open(AreaDetailDialog, {
      data: area,
      minWidth: 900
    })
  }

  onEdit(e, area: Area) {
    e.stopPropagation();
    let dialogData = this.dialog.open(AreaEditDialog, {
      data: area,
      minWidth: 450
    })
    dialogData.afterClosed().subscribe(res => {
      this.getList();
    })
  }

  onDelete(e, id) {
    e.stopPropagation();
    let dialogData = this.dialog.open(ConfirmDialog, {
      data: {
        title: 'Delete',
        content: 'Do you want delete it?'
      },
      minWidth: 450
    });
    dialogData.afterClosed().subscribe(res => {
      if(res) {
        this.dialog.open(SpinnerDialog);
        this.areaService.delete(id).subscribe(res => {
          this.getList();
          this.dialog.closeAll();
        })
      }
    })
  }
}
