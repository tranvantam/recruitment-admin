import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { CvService } from 'src/app/modules/cv/services/cv.service';
import { Cv } from 'src/app/core/models/cv';
import { CvCreateDialog } from './dialogs/cv-create/cv-create.dialog';
import { CvDetailDialog } from './dialogs/cv-detail/cv-detail.dialog';
import { CvEditDialog } from './dialogs/cv-edit/cv-edit.dialog';
import { ConfirmDialog } from 'src/app/shared/dialogs/confirm/confirm.dialog';
import { SpinnerDialog } from 'src/app/shared/dialogs/spinner/spinner.dialog';

@Component({
  selector: 'app-cv',
  templateUrl: './cv.page.html',
  styles: [
  ]
})
export class CvPage implements OnInit {
  arrCv: Array<Cv>;
  cv: Cv;

  constructor(
    private cvService: CvService,
    public dialog: MatDialog,  
  ) { }

  ngOnInit(): void {
    this.getList();
  }

  getList() {
    this.cvService.getList().subscribe(res => {
      this.arrCv = res;
    })
  }

  onCreate() {
    let dialogData = this.dialog.open(CvCreateDialog, {
      minWidth: 450
    });
    dialogData.afterClosed().subscribe(res => {
      this.getList();
    })
  }

  onDetail(cv: Cv) {
    this.dialog.open(CvDetailDialog, {
      data: cv,
      minWidth: 900
    })
  }

  onEdit(e, cv: Cv) {
    e.stopPropagation();
    let dialogData = this.dialog.open(CvEditDialog, {
      data: cv,
      minWidth: 450
    })
    dialogData.afterClosed().subscribe(res => {
      this.getList();
    })
  }

  onDelete(e, id) {
    e.stopPropagation();
    let dialogData = this.dialog.open(ConfirmDialog, {
      data: {
        title: 'Delete',
        content: 'Do you want delete it?'
      },
      minWidth: 450
    });
    dialogData.afterClosed().subscribe(res => {
      if(res) {
        this.dialog.open(SpinnerDialog);
        this.cvService.delete(id).subscribe(res => {
          this.getList();
          this.dialog.closeAll();
        })
      }
    })
  }
}
