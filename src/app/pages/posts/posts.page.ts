import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { PostsService } from 'src/app/modules/posts/services/posts.service';
import { Posts } from 'src/app/core/models/posts';
import { PostsDetailDialog } from 'src/app/pages/posts/dialogs/posts-detail/posts-detail.dialog';
import { PostsCreateDialog } from 'src/app/pages/posts/dialogs/posts-create/posts-create.dialog';
import { AreaService } from 'src/app/modules/area/services/area.service';
import { CategoryService } from 'src/app/modules/category/services/category.service';
import { ConfirmDialog } from 'src/app/shared/dialogs/confirm/confirm.dialog';
import { PostsEditDialog } from './dialogs/posts-edit/posts-edit.dialog';
import { SpinnerDialog } from 'src/app/shared/dialogs/spinner/spinner.dialog';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.page.html',
  styles: [
  ]
})
export class PostsPage implements OnInit {
  arrPosts: Array<Posts>;
  posts: Posts;
  
  constructor(
    private postsService: PostsService, 
    public dialog: MatDialog,
    private areaService: AreaService,
    private cateService: CategoryService
    ) { }

  ngOnInit(): void {    
    this.getList();
  }
  
  getList() {
    this.postsService.getList().subscribe(res => {
        this.arrPosts = res;         
      }
    )
  }

  onCreate() {    
    let promiseGetCate = new Promise((resolve, reject) => {
      this.cateService.getList().subscribe(res => { 
        let data: any = {};
        let arrId = res.map(cate => cate);        
        data.cateId = arrId;
        resolve(data);
      })      
    })
    let promiseGetArea = new Promise((resolve, reject) => {
      this.areaService.getList().subscribe(res => {
        let data: any = {};
        let arrId = res.map(area => area);
        data.areaId = arrId;
        resolve(data);
      }) 
    })
    Promise.all([promiseGetArea, promiseGetCate]).then(res => {      
      let dialogData = this.dialog.open(PostsCreateDialog, {
        data: res,
      });
      dialogData.afterClosed().subscribe(res => {
        this.getList();
      })
    })     
  }

  onDetail(posts: Posts) {
    this.dialog.open(PostsDetailDialog, {
      data: posts,
      minWidth: 1100
    })
  }

  onEdit(e, posts: Posts) {
    e.stopPropagation();
    let promiseGetCate = new Promise((resolve, reject) => {
      this.cateService.getList().subscribe(res => {
        let data: any = {};
        let arrId = res.map(cate => cate);
        data.cateId = arrId;
        resolve(data);
      })      
    })
    let promiseGetArea = new Promise((resolve, reject) => {
      this.areaService.getList().subscribe(res => {
        let data: any = {};
        let arrId = res.map(area => area);
        data.areaId = arrId;
        resolve(data);
      }) 
    })
    Promise.all([promiseGetArea, promiseGetCate]).then(res => {      
      res.push({postsData: posts})
      let dialogData = this.dialog.open(PostsEditDialog, {
        data: res,
      });
      dialogData.afterClosed().subscribe(res => {
        this.getList();
      })
    })     
  }

  onDelete(e, id) {   
    e.stopPropagation();
    let dialogData = this.dialog.open(ConfirmDialog, {
      data: {
        title: 'Delete',
        content: 'Do you want delete it?'
      },
      minWidth: 450
    });
    dialogData.afterClosed().subscribe(res => {
      if(res) {
        this.dialog.open(SpinnerDialog);
        this.postsService.delete(id).subscribe(res => {
          this.getList();
          this.dialog.closeAll();
        })
      }
    })
  }
}
