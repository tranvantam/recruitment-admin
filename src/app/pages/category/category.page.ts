import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { CategoryService } from 'src/app/modules/category/services/category.service';
import { Category } from 'src/app/core/models/category';
import { CategoryCreateDialog } from './dialogs/category-create/category-create.dialog';
import { CategoryDetailDialog } from './dialogs/category-detail/category-detail.dialog';
import { CategoryEditDialog } from './dialogs/category-edit/category-edit.dialog';
import { ConfirmDialog } from 'src/app/shared/dialogs/confirm/confirm.dialog';
import { SpinnerDialog } from 'src/app/shared/dialogs/spinner/spinner.dialog';

@Component({
  selector: 'app-category',
  templateUrl: './category.page.html',
  styles: [
  ]
})
export class CategoryPage implements OnInit {
  arrCate: Array<Category>;
  cate: Category;

  constructor(private cateService: CategoryService, public dialog: MatDialog) { }

  ngOnInit(): void {
    this.getList();
  }

  getList() {
    this.cateService.getList().subscribe(res => {
      this.arrCate = res;          
    })
  }

  onCreate() {
    let dialogData = this.dialog.open(CategoryCreateDialog, {
      minWidth: 450
    });
    dialogData.afterClosed().subscribe(res => {
      this.getList();
    })
  }

  onDetail(cate: Category) {
    this.dialog.open(CategoryDetailDialog, {
      data: cate,
      minWidth: 900
    })
  }

  onEdit(e, cate: Category) {
    e.stopPropagation();
    let dialogData = this.dialog.open(CategoryEditDialog, {
      data: cate,
      minWidth: 450
    })
    dialogData.afterClosed().subscribe(res => {
      this.getList();
    })
  }

  onDelete(e, id) {
    e.stopPropagation();
    let dialogData = this.dialog.open(ConfirmDialog, {
      data: {
        title: 'Delete',
        content: 'Do you want delete it?'
      },
      minWidth: 450
    });
    dialogData.afterClosed().subscribe(res => {
      if(res) {
        this.dialog.open(SpinnerDialog);
        this.cateService.delete(id).subscribe(res => {
          this.getList();
          this.dialog.closeAll();
        })
      }
    })
  }
}
